package cn.yesway.baicpre.tbox.gateway;

import java.net.Socket;

public class SocketClient {
	public static void main(String[] args) {
		String serverip = "127.0.0.1";
		int serverport = 9999;
		try {
			System.out.println("开始与服务ip：" + serverip + ",端口:" + serverport + "建立连接");
			Socket socket = new Socket(serverip, serverport);
			socket.setSoTimeout(60 * 1000);
			System.out.println("建立连接成功");
			SocketClientHanlder clienthanlder = new SocketClientHanlder(socket);
			clienthanlder.start();
		} catch (Exception e) {
			System.out.println("与服务端建立连接时发生错误：" + e);
		}
	}
}
