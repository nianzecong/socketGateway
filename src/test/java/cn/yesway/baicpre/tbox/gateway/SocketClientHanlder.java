package cn.yesway.baicpre.tbox.gateway;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;

import cn.yesway.tsp.access.util.ByteUtils;


public class SocketClientHanlder extends Thread {
	private BufferedInputStream in;

	private BufferedOutputStream out;

	private Socket socket;

	public SocketClientHanlder(Socket socket) {
		this.socket = socket;
	}

	public void run() {
		try {
			in = new BufferedInputStream(socket.getInputStream());
			out = new BufferedOutputStream(socket.getOutputStream());

			//String byteArrayStr = "464c432e01007000600199bba85872966779a9ac867d20e21805fe434e661e77432083ebb051cea8a8ab384c57586a0821560a71d645c40d5a79ca546d6aa514beaa330a317ee527a7f159feebe46d2de89094ebd7d4ffeaf1d7b627117bd7ce4d05d2a330b303e594a971ca73c71c49a90f0912296a6a30bfc86a";
			String byteArrayStr = "00021234";
			System.out.println("发送信息：0x" + byteArrayStr);
			byte[] bytes = ByteUtils.getBytesFromHex(byteArrayStr);
			out.write(bytes);
			out.flush();
			
			byte[] data = new byte[1024 * 8];
			int length = in.read(data);
			if (length == -1) {
				System.out.println("接受消息的包头时输入流已结束!");
			} else {
				if (length < 1024 * 8) {
					byte[] temp = new byte[length];
					ByteUtils.addBytes(temp, 0, data, length);
					System.out.println("接受到消息：0x" + ByteUtils.getHexString(temp));
				} else {
					System.out.println("接受到消息：0x" + ByteUtils.getHexString(data));
				}
			}
		} catch (Exception e) {
			System.out.println("线程处理socket时发生错误：" + e);
		}
	}
}
