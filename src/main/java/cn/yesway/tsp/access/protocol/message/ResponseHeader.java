package cn.yesway.tsp.access.protocol.message;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import cn.yesway.tsp.access.util.ByteUtils;

public class ResponseHeader implements Serializable{

	private static final long serialVersionUID = 1107082347630619220L;
	
	private int errcode;
	private String errmsg = "";
	
	/**
	 * 将消息头编码
	 * @param respHeader
	 * @return
	 */
	public static byte[] encode(ResponseHeader respHeader){
		if(respHeader == null) {
			return null;
		}
		respHeader.errmsg = StringUtils.isBlank(respHeader.errmsg) ? "" : respHeader.errmsg;
		byte[] msg = respHeader.errmsg.getBytes();
		byte[] bytes = new byte[msg.length + 1];
		ByteUtils.addByte(bytes, 0, (byte)respHeader.errcode);
		ByteUtils.addBytes(bytes, 1, msg);
		return bytes;
	}
	
	public int getErrcode() {
		return errcode;
	}
	public void setErrcode(int errcode) {
		this.errcode = errcode;
	}
	
	public String getErrmsg() {
		return errmsg;
	}
	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("========= Response Header =============\n")
			.append("{errcode:").append(errcode).append(", ");
		if (errmsg != null)
			builder.append("errmsg:").append(errmsg);
		builder.append("}\n");
		return builder.toString();
	}
	
	public static void main(String[] args) {
		ResponseHeader header = new ResponseHeader();
		header.errcode = 0;
		header.errmsg = "";
		System.out.println(header);
		System.out.println("0x" + ByteUtils.getHexString(ResponseHeader.encode(header)));
	}
	
}
