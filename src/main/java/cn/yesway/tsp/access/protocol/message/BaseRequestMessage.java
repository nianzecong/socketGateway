package cn.yesway.tsp.access.protocol.message;

import java.io.Serializable;

import cn.yesway.tsp.access.util.ByteUtils;

public abstract class BaseRequestMessage implements Serializable{

	private static final long serialVersionUID = 8394343243794010232L;
	
	protected RequestHeader header;

	public static class Command{
		public static final byte[] CONFIG_REQUEST = ByteUtils.getBytesFromHex("1001");
		public static final byte[] CONFIG_RESPONSE = ByteUtils.getBytesFromHex("9001");
	}
	
	/**
	 * 消息解码
	 * @param bytes
	 * @return
	 */
	public abstract BaseRequestMessage decode(byte[] bytes);

	/**
	 * 消息编码
	 * @param message
	 * @return
	 */
	public abstract byte[] encode();

	public RequestHeader getHeader() {
		return header;
	}

	public void setHeader(RequestHeader header) {
		this.header = header;
	}

}
