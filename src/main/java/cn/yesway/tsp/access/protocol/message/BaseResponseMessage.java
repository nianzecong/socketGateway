package cn.yesway.tsp.access.protocol.message;

import java.io.Serializable;

public abstract class BaseResponseMessage implements Serializable{

	private static final long serialVersionUID = 8394343243794010232L;
	
	private ResponseHeader header;

	/**
	 * 消息解码
	 * @param bytes
	 * @return
	 */
	public abstract BaseResponseMessage decode(byte[] bytes);

	/**
	 * 消息编码
	 * @param message
	 * @return
	 */
	public abstract byte[] encode(BaseResponseMessage message);

	public ResponseHeader getHeader() {
		return header;
	}

	public void setHeader(ResponseHeader header) {
		this.header = header;
	}

	
}
