package cn.yesway.tsp.access.protocol.message;

import java.util.UUID;

import cn.yesway.tsp.access.util.ByteUtils;

public class ConfigRequest extends BaseRequestMessage {
	private static final long serialVersionUID = -6220024607279484904L;
	
	private static final int LENGTH = RequestHeader.LENGTH + 32 + 32 + 32;
	// 硬件版本号
	private String hardversion;
	// 固件版本号
	private String firmversion;
	// 软件版本号
	private String softversion;

	@Override
	public BaseRequestMessage decode(byte[] bytes) {
		this.setHeader(RequestHeader.decode(bytes));
		int pos = RequestHeader.LENGTH;
		this.setHardversion(new String(ByteUtils.readBytes(bytes, pos, 32)).trim());
		pos += 32;
		this.setFirmversion(new String(ByteUtils.readBytes(bytes, pos, 32)).trim());
		pos += 32;
		this.setSoftversion(new String(ByteUtils.readBytes(bytes, pos, 32)).trim());
		return this;
	}

	@Override
	public byte[] encode(){
		byte[] bytes = new byte[ConfigRequest.LENGTH];
		int pos = 0;
		pos = ByteUtils.addBytes(bytes, pos, RequestHeader.encode(this.header));
		pos = ByteUtils.addBytes(bytes, pos, this.hardversion.getBytes());
		pos = ByteUtils.addBytes(bytes, pos, this.firmversion.getBytes());
		pos = ByteUtils.addBytes(bytes, pos, this.softversion.getBytes());
		return bytes;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder
		.append("========= ConfigRequest ============\n")
		.append(super.getHeader().toString())
		.append("\n==== Request body :\n")
		.append("{\n")
		.append("  hardversion:").append(hardversion)
		.append(",\n  firmversion:").append(firmversion)
		.append(",\n  softversion:").append(softversion)
		.append("\n}")
		.append("\n====================================");
		return builder.toString();
	}

	/**
	 * @return the hardversion
	 */
	public String getHardversion() {
		return hardversion;
	}

	/**
	 * @param hardversion the hardversion to set
	 */
	public void setHardversion(String hardversion) {
		this.hardversion = hardversion;
	}

	/**
	 * @return the firmversion
	 */
	public String getFirmversion() {
		return firmversion;
	}

	/**
	 * @param firmversion the firmversion to set
	 */
	public void setFirmversion(String firmversion) {
		this.firmversion = firmversion;
	}

	/**
	 * @return the softversion
	 */
	public String getSoftversion() {
		return softversion;
	}

	/**
	 * @param softversion the softversion to set
	 */
	public void setSoftversion(String softversion) {
		this.softversion = softversion;
	}

	public static void main(String[] args) {
		ConfigRequest msg = new ConfigRequest();
		RequestHeader header = new RequestHeader();
		header.setCommand(BaseRequestMessage.Command.CONFIG_REQUEST);
		header.setCreatTime("201508101731");
		header.setDeviceAuthCode("12345678901234567890");
		header.setDeviceId("12345678901234567890");
		header.setDeviceSource("12345678901234567890");
		header.setDeviceType(1);
		header.setTripId(UUID.randomUUID().toString().replaceAll("-", ""));
		header.setVin("12345678901234567890");
		
		msg.setHardversion("1234567890123456789012345678900");
		msg.setFirmversion("12345678901234567890123456789002");
		msg.setSoftversion("12345678901234567890123456789003");
		msg.setHeader(header);
		
		byte[] bytes = msg.encode();
		System.out.println(ByteUtils.getHexString(bytes));
		System.out.println(new ConfigRequest().decode(bytes));
	}
	
}
