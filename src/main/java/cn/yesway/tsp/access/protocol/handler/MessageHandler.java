package cn.yesway.tsp.access.protocol.handler;

import java.lang.reflect.Constructor;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cn.yesway.tsp.access.protocol.message.BaseRequestMessage;
import cn.yesway.tsp.access.protocol.message.ConfigRequest;
import cn.yesway.tsp.access.util.ByteUtils;

public class MessageHandler {

	private static MessageHandler handler;
	
	private MessageHandler(){};
	
	public static MessageHandler getInstance(){
		if(handler == null){
			handler = new MessageHandler();
		}
		return handler;
	}
	
	private static Map<byte[], Class<?>> messageMap = new HashMap<byte[], Class<?>>(){
		private static final long serialVersionUID = -6516334304863050154L;
		{
			put(BaseRequestMessage.Command.CONFIG_REQUEST, ConfigRequest.class);
		}
	};

	public BaseRequestMessage decode(byte[] data){
		int pos = 8;// 协议包长度2 + 协议版本号2 + 传输流水号4
		byte[] command = ByteUtils.readBytes(data, pos, 2);
		pos += 4;
		byte[] msgData = ByteUtils.readBytes(data, pos, data.length - pos);
		BaseRequestMessage msg;
		Constructor<?> c;
		try {
			c = messageMap.get(command).getConstructor();
			msg = (BaseRequestMessage)c.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static String getSwiftNo(){
		return ByteUtils.getHexString((int)new Date().getTime());
	}
	
}
