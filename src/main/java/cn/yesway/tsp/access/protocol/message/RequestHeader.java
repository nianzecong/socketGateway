package cn.yesway.tsp.access.protocol.message;

import java.io.Serializable;
import java.util.UUID;

import cn.yesway.tsp.access.util.ByteUtils;

public class RequestHeader implements Serializable {

	private static final long serialVersionUID = -1125550299669470015L;
	public static final int LENGTH = 2 + 20 + 20 + 1 + 20 + 20 + 32 + 6;
	
	// 命令字，唯一标识本消息包的命令类型
	private byte[] command;
	// 设备ID，设备的唯一编号，需要在平台进行授权
	private String deviceId;
	// 设备验证码，对设备身份进行验证
	private String deviceAuthCode;
	// 设备类型，0：OBD，1：车机
	private int deviceType;
	// 设备来源，标明设备的来源，如厂家、企业定制等，由平台进行分配和授权，设备出厂时内置
	private String deviceSource;
	// 车架号，车辆类型唯一识别号，从车辆读取
	private String vin;
	// 形成ID，需要设备生成。在任何情况下，如系统出厂、使用过程中、卸载重装、升级、恢复出厂设置等，同一个设备生成的行程ID必须确保是唯一的，不能重复。
	private String tripId;
	// 数据产生时间，BCD[6],8421编码数组，时间格式为yyyyMMddHHmmss。可表达为0x20150430083001
	private String creatTime;
	
	/**
	 * 解码
	 * 
	 * @param bytes
	 *            消息包内容
	 * @return
	 */
	public static RequestHeader decode(byte[] bytes) {
		RequestHeader header = new RequestHeader();
		int pos = 0;
		header.command = ByteUtils.readBytes(bytes, pos, 2);
		pos += 2;
		header.deviceId = new String(ByteUtils.readBytes(bytes, pos, 20)).trim();
		pos += 20;
		header.deviceAuthCode = new String(ByteUtils.readBytes(bytes, pos, 20)).trim();
		pos += 20;
		header.deviceType = ByteUtils.readByte(bytes, pos);
		pos += 1;
		header.deviceSource = new String(ByteUtils.readBytes(bytes, pos, 20)).trim();
		pos += 20;
		header.vin = new String(ByteUtils.readBytes(bytes, pos, 20)).trim();
		pos += 20;
		header.tripId = new String(ByteUtils.readBytes(bytes, pos, 32)).trim();
		pos += 32;
		header.creatTime = ByteUtils.getHexString(ByteUtils.readBytes(bytes, pos, 6));
		return header;
	}

	/**
	 * 编码
	 * 
	 * @param header
	 *            消息头
	 * @return
	 */
	public static byte[] encode(RequestHeader header) {
		if(header == null){return null;}
		byte[] bytes = new byte[RequestHeader.LENGTH];
		int pos = 0;
		pos = ByteUtils.addBytes(bytes, pos, header.command);
		pos = ByteUtils.addBytes(bytes, pos, header.deviceId.getBytes());
		pos = ByteUtils.addBytes(bytes, pos, header.deviceAuthCode.getBytes());
		pos = ByteUtils.addByte(bytes, pos, (byte)header.deviceType);
		pos = ByteUtils.addBytes(bytes, pos, header.deviceSource.getBytes());
		pos = ByteUtils.addBytes(bytes, pos, header.vin.getBytes());
		pos = ByteUtils.addBytes(bytes, pos, header.tripId.getBytes());
		pos = ByteUtils.addBytes(bytes, pos, ByteUtils.getBytesFromHex(header.creatTime));
		return bytes;
	}

	public byte[] getCommand() {
		return command;
	}
	public void setCommand(byte[] command) {
		this.command = command;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getDeviceAuthCode() {
		return deviceAuthCode;
	}
	public void setDeviceAuthCode(String deviceAuthCode) {
		this.deviceAuthCode = deviceAuthCode;
	}
	public int getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(int deviceType) {
		this.deviceType = deviceType;
	}
	public String getDeviceSource() {
		return deviceSource;
	}
	public void setDeviceSource(String deviceSource) {
		this.deviceSource = deviceSource;
	}
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	public String getTripId() {
		return tripId;
	}

	public void setTripId(String tripId) {
		this.tripId = tripId;
	}

	public String getCreatTime() {
		return creatTime;
	}
	public void setCreatTime(String creatTime) {
		this.creatTime = creatTime;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("==== Request Header :\n");
		builder.append("{\ncommand:\t").append(ByteUtils.getHexString(command)).append(", \n");
		if (deviceId != null)
			builder.append("deviceId:\t").append(deviceId).append(", \n");
		if (deviceAuthCode != null)
			builder.append("deviceAuthCode:\t").append(deviceAuthCode).append(", \n");
		builder.append("deviceType:\t").append(deviceType).append(", \n");
		if (deviceSource != null)
			builder.append("deviceSource:\t").append(deviceSource).append(", \n");
		if (vin != null)
			builder.append("vin:\t\t").append(vin).append(", \n");
		if (tripId != null)
			builder.append("journeyId:\t").append(tripId).append(", \n");
		if (creatTime != null)
			builder.append("creatTime:\t").append(creatTime);
		builder.append("\n}");
		return builder.toString();
	}

	public static void main(String[] args) {
		RequestHeader header = new RequestHeader();
		byte[] command = {(byte)0x00, (byte)0x02};
		header.setCommand(command);
		header.setCreatTime("201508101731");
		header.setDeviceAuthCode("12345678901234567891");
		header.setDeviceId("12345678901234567892");
		header.setDeviceSource("12345678901234567893");
		header.setDeviceType(1);
		header.setTripId(UUID.randomUUID().toString().replaceAll("-", "").toUpperCase());
		header.setVin("12345678901234567894");
		byte[] data = RequestHeader.encode(header);
		System.out.println(ByteUtils.getHexString(data));
		System.out.println(RequestHeader.decode(data));
	}
	
}
