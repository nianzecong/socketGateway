package cn.yesway.tsp.access.exception;

public class InvalidDataException extends BaseException{

	private static final long serialVersionUID = -5011894722147287513L;

	public InvalidDataException(String errmsg) {
		super(BaseException.ErrCode.INVALID_DATA, errmsg);
	}

}
