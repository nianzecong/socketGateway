package cn.yesway.tsp.access.exception;

public class BaseException extends Exception {

	private static final long serialVersionUID = 2201033136381998072L;

	private int errcode;
	private String errmsg;

	/**
	 * errCode常量
	 */
	public static class ErrCode {
		// 无错误
		public static final int SUCCESS = 0;
		// 设备不存在
		public static final int DEVICE_NOT_EXIST = 1;
		// 设备验证错误
		public static final int DEVICE_AUTH_ERROR = 2;
		// 参数不合法
		public static final int INVALID_PARAMS = 3;
		// 内部错误
		public static final int INNER_ERROR = 4;
		// 数据包格式验证错误
		public static int INVALID_DATA = 5;
	}

	@SuppressWarnings("unused")
	private BaseException() {
	}

	public BaseException(int errcode, String errmsg) {
		super(errmsg);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("{errcode:").append(errcode).append(",\n  errmsg:").append(errmsg).append("}");
		return builder.toString();
	}

	/**
	 * @return the errcode
	 */
	public int getErrcode() {
		return errcode;
	}

	/**
	 * @param errcode
	 *            the errcode to set
	 */
	public void setErrcode(int errcode) {
		this.errcode = errcode;
	}

	/**
	 * @return the errmsg
	 */
	public String getErrmsg() {
		return errmsg;
	}

	/**
	 * @param errmsg
	 *            the errmsg to set
	 */
	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

}
