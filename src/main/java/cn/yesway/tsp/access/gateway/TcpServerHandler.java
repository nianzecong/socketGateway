package cn.yesway.tsp.access.gateway;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.yesway.tsp.access.util.ByteUtils;
import cn.yesway.tsp.access.util.HttpClientUtils;

public class TcpServerHandler extends ChannelInboundHandlerAdapter {
	private static final Logger log = LoggerFactory.getLogger(TcpServerHandler.class);

	private String serviceUrl;

	public TcpServerHandler(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		log.debug(ctx.channel().remoteAddress() + ": Client establishes a connection");
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		ByteBuf in = (ByteBuf) msg;
		byte[] bytes = new byte[in.readableBytes()];
		in.readBytes(bytes);
		log.debug(ctx.channel().remoteAddress() + ": Received message: 0x" + ByteUtils.getHexString(bytes));
		//TODO
		
		ctx.writeAndFlush(Unpooled.wrappedBuffer(ByteUtils.getBytesFromHex("111111")));
	}

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		ctx.flush();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		log.error(ctx.channel().remoteAddress() + ": The connection error: " + cause.getMessage());
		ctx.close();
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		log.debug(ctx.channel().remoteAddress() + ": Client disconnected");
	}

}
