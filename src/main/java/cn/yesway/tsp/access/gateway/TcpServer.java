package cn.yesway.tsp.access.gateway;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.yesway.tsp.access.util.AppConfig;

public class TcpServer {
	private static final Logger log = LoggerFactory.getLogger(TcpServer.class);

	public static void main(String[] args) throws Exception {
		log.info("Start loading configuration information");
		AppConfig.configure(TcpServer.class.getResource("/appconfig.xml").getFile());
		log.info("Loading configuration information to complete");

		final int port = Integer.parseInt(AppConfig.getParameter("port"));
		final int lengthFieldOffset = Integer.parseInt(AppConfig.getParameter("lengthFieldOffset"));
		final int lengthFieldLength = Integer.parseInt(AppConfig.getParameter("lengthFieldLength"));
		final int lengthAdjustment = Integer.parseInt(AppConfig.getParameter("lengthAdjustment"));
		final int initialBytesToStrip = Integer.parseInt(AppConfig.getParameter("initialBytesToStrip"));
		
		final String serviceUrl = AppConfig.getParameter("serviceUrl");
		
		log.info("Listen Port: " + port);
		log.info("Service Processing Address: " + serviceUrl);

		EventLoopGroup bossGroup = new NioEventLoopGroup();
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		try {
			ServerBootstrap b = new ServerBootstrap();
			b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class).childHandler(new ChannelInitializer<SocketChannel>() {
				@Override
				public void initChannel(SocketChannel ch) throws Exception {
					/*
					 * lengthFieldOffset, lengthFieldLength 从offset位置，截取length长度作为消息包（在lengthField之后）的总长度
					 * lengthAdjustment，长度调节，例如：lengthField标识的长度不包括一字节长度的checksum，则lengthAdjustment为1，使得可以获取checksum,
					 * initialBytesToStrip，数据头部切掉的字节数，decoder获取的是完整的数据包，若业务处理模块不需要数据开头固定的字节“##”，则可设置为2跳过
					 * */
					ch.pipeline().addLast("decoder", new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, lengthFieldOffset, lengthFieldLength, lengthAdjustment, initialBytesToStrip));
					ch.pipeline().addLast("handler", new TcpServerHandler(serviceUrl));
				}
			}).option(ChannelOption.SO_BACKLOG, 128).childOption(ChannelOption.SO_KEEPALIVE, true);

			log.info("Start Service");
			b.bind(port).sync().channel().closeFuture().sync();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();
		}
		log.info("End Service");
	}
}
